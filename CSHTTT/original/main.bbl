\begin{thebibliography}{10}

\bibitem{devlin2018bert}
Jacob Devlin, Ming-Wei Chang, Kenton Lee, and Kristina Toutanova,
\newblock ``Bert: Pre-training of deep bidirectional transformers for language
  understanding,''
\newblock {\em arXiv preprint arXiv:1810.04805}, 2018.

\bibitem{karpukhin2020dense}
Vladimir Karpukhin, Barlas O{\u{g}}uz, Sewon Min, Patrick Lewis, Ledell Wu,
  Sergey Edunov, Danqi Chen, and Wen-tau Yih,
\newblock ``Dense passage retrieval for open-domain question answering,''
\newblock {\em arXiv preprint arXiv:2004.04906}, 2020.

\bibitem{xiong2020approximate}
Lee Xiong, Chenyan Xiong, Ye~Li, Kwok-Fung Tang, Jialin Liu, Paul Bennett,
  Junaid Ahmed, and Arnold Overwijk,
\newblock ``Approximate nearest neighbor negative contrastive learning for
  dense text retrieval,''
\newblock {\em arXiv preprint arXiv:2007.00808}, 2020.

\bibitem{qu2020rocketqa}
Yingqi Qu, Yuchen Ding, Jing Liu, Kai Liu, Ruiyang Ren, Wayne~Xin Zhao, Daxiang
  Dong, Hua Wu, and Haifeng Wang,
\newblock ``Rocketqa: An optimized training approach to dense passage retrieval
  for open-domain question answering,''
\newblock {\em arXiv preprint arXiv:2010.08191}, 2020.

\bibitem{liu2022retromae}
Zheng Liu and Yingxia Shao,
\newblock ``Retromae: Pre-training retrieval-oriented transformers via masked
  auto-encoder,''
\newblock {\em arXiv preprint arXiv:2205.12035}, 2022.

\bibitem{nguyen2016ms}
Tri Nguyen, Mir Rosenberg, Xia Song, Jianfeng Gao, Saurabh Tiwary, Rangan
  Majumder, and Li~Deng,
\newblock ``Ms marco: A human generated machine reading comprehension
  dataset,''
\newblock in {\em CoCo@ NIPs}, 2016.

\bibitem{louis2021statutory}
Antoine Louis and Gerasimos Spanakis,
\newblock ``A statutory article retrieval dataset in french,''
\newblock {\em arXiv preprint arXiv:2108.11792}, 2021.

\bibitem{daudert-etal-2018-leveraging}
Tobias Daudert, Paul Buitelaar, and Sapna Negi,
\newblock ``Leveraging news sentiment to improve microblog sentiment
  classification in the financial domain,''
\newblock in {\em Proceedings of the First Workshop on Economics and Natural
  Language Processing}, Melbourne, Australia, July 2018, pp. 49--54,
  Association for Computational Linguistics.

\bibitem{chen2021finqa}
Zhiyu Chen, Wenhu Chen, Charese Smiley, Sameena Shah, Iana Borova, Dylan
  Langdon, Reema Moussa, Matt Beane, Ting-Hao Huang, Bryan Routledge, et~al.,
\newblock ``Finqa: A dataset of numerical reasoning over financial data,''
\newblock {\em arXiv preprint arXiv:2109.00122}, 2021.

\bibitem{jorgensen-etal-2023-multifin}
Rasmus J{\o}rgensen, Oliver Brandt, Mareike Hartmann, Xiang Dai, Christian
  Igel, and Desmond Elliott,
\newblock ``{M}ulti{F}in: A dataset for multilingual financial {NLP},''
\newblock in {\em Findings of the Association for Computational Linguistics:
  EACL 2023}, Dubrovnik, Croatia, May 2023, pp. 894--909, Association for
  Computational Linguistics.

\bibitem{daudert-ahmadi-2019-cofif}
Tobias Daudert and Sina Ahmadi,
\newblock ``{C}o{F}i{F}: A corpus of financial reports in {F}rench language,''
\newblock in {\em Proceedings of the First Workshop on Financial Technology and
  Natural Language Processing}, Macao, China, Aug. 2019, pp. 21--26.

\bibitem{handschke-etal-2018-corpus}
Sebastian~G.M. H{\"a}ndschke, Sven Buechel, Jan Goldenstein, Philipp Poschmann,
  Tinghui Duan, Peter Walgenbach, and Udo Hahn,
\newblock ``A corpus of corporate annual and social responsibility reports: 280
  million tokens of balanced organizational writing,''
\newblock in {\em Proceedings of the First Workshop on Economics and Natural
  Language Processing}, Melbourne, Australia, July 2018, pp. 20--31,
  Association for Computational Linguistics.

\bibitem{wilson-etal-2016-creation}
Shomir Wilson, Florian Schaub, Aswarth~Abhilash Dara, Frederick Liu, Sushain
  Cherivirala, Pedro Giovanni~Leon, Mads Schaarup~Andersen, Sebastian Zimmeck,
  Kanthashree~Mysore Sathyendra, N.~Cameron Russell, Thomas~B. Norton, Eduard
  Hovy, Joel Reidenberg, and Norman Sadeh,
\newblock ``The creation and analysis of a website privacy policy corpus,''
\newblock in {\em Proceedings of the 54th Annual Meeting of the Association for
  Computational Linguistics (Volume 1: Long Papers)}, Berlin, Germany, Aug.
  2016, pp. 1330--1340, Association for Computational Linguistics.

\bibitem{robertson1995okapi}
Stephen Robertson, S.~Walker, S.~Jones, M.~M. Hancock-Beaulieu, and M.~Gatford,
\newblock ``Okapi at trec-3,''
\newblock in {\em Overview of the Third Text REtrieval Conference (TREC-3)}.
  January 1995, pp. 109--126, Gaithersburg, MD: NIST.

\bibitem{zhao2020TermTree}
Min Zhao, Huapeng Qin, Guoxin Zhang, Yajuan Lyu, and Yong Zhu,
\newblock ``Termtree and knowledge annotation framework for chinese language
  understanding,''
\newblock Tech. {R}ep. TR:2020-KG-TermTree, Baidu, Inc., 2020.

\bibitem{simbert}
Jianlin Su,
\newblock ``Simbert: Integrating retrieval and generation into bert,''
\newblock Tech. {R}ep., 2020.

\bibitem{cui-etal-2020-revisiting}
Yiming Cui, Wanxiang Che, Ting Liu, Bing Qin, Shijin Wang, and Guoping Hu,
\newblock ``Revisiting pre-trained models for {C}hinese natural language
  processing,''
\newblock in {\em Findings of the Association for Computational Linguistics:
  EMNLP 2020}, Online, Nov. 2020, pp. 657--668, Association for Computational
  Linguistics.

\bibitem{zhang2021mengzi}
Zhuosheng Zhang, Hanqing Zhang, Keming Chen, Yuhang Guo, Jingyun Hua, Yulong
  Wang, and Ming Zhou,
\newblock ``Mengzi: Towards lightweight yet ingenious pre-trained models for
  chinese,''
\newblock {\em arXiv preprint arXiv:2110.06696}, 2021.

\bibitem{text2vec}
Ming Xu,
\newblock ``Text2vec: Text to vector toolkit,''
  \url{https://github.com/shibing624/text2vec}, 2023.

\end{thebibliography}
